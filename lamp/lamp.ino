#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>

#define PIN D3
Adafruit_NeoPixel strip = Adafruit_NeoPixel(16, PIN, NEO_GRB + NEO_KHZ800);
// Update these with values suitable for your network.
//const char* ssid = "PNO";
//const char* password = "vxt17HUS";
//const char* mqtt_server = "192.168.1.46";
const char* ssid = "NTIElev";
const char* password = "7pQA2gy59HbK";
const char* mqtt_server = "192.168.9.164";




WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
int rgb[3]={0,0,0};




int array_to_num(int arr[],int n){
	char str[6][3];
	int i;
	char number[13] = {'\n'};

	for(i=0;i<n;i++) sprintf(str[i],"%d",arr[i]);
	for(i=0;i<n;i++)strcat(number,str[i]);

	i = atoi(number);
	return i;
}





void setup_wifi() {

	delay(10);
	// We start by connecting to a WiFi network
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);

	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	randomSeed(micros());

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
}
void callback(String topic, byte* payload, unsigned int length)
{
	Serial.println(topic);
	int input[length];


	for (int i = 0; i < length; i++)
	{
		Serial.print((char)payload[i]);
		input[i]=(int)payload[i]-48;
	}
	int intIn;
	intIn = array_to_num(input,length);
	Serial.println(intIn);

	if (topic=="r") {
		/* code */
		Serial.print(" * ");
		Serial.print("R: ");
		Serial.print(" * ");
		rgb[0]=intIn;
		Serial.println(intIn);
	} else if (topic=="g") {
		/* code */
		Serial.print(" * ");
		Serial.print("G: ");
		Serial.print(" * ");
		rgb[1]=intIn;
		Serial.println(intIn);
	} else if (topic=="b") {
		/* code */
		Serial.print(" * ");
		Serial.print("B: ");
		Serial.print(" * ");
		rgb[2]=intIn;
		Serial.println(intIn);
	}else{
		Serial.println("ERROR 1");
	}
}
void reconnect() {
	// Loop until we're reconnected
	while (!client.connected()) {
		Serial.print("Attempting MQTT connection...");
		// Create a random client ID
		String clientId = "ESP8266Client-";
		clientId += String(random(0xffff), HEX);
		// Attempt to connect
		if (client.connect(clientId.c_str())) {
			Serial.println("connected");
			// Once connected, publish an announcement...
			client.publish("outTopic", "hello world i'm a RGB LAMP");
			// ... and resubscribe
			client.subscribe("inTopic");
			client.subscribe("r");
			client.subscribe("g");
			client.subscribe("b");
		} else {
			Serial.print("failed, rc=");
			Serial.print(client.state());
			Serial.println(" try again in 5 seconds");
			// Wait 5 seconds before retrying
			delay(5000);
		}
	}
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
	for(uint16_t i=0; i<strip.numPixels(); i++) {
		strip.setPixelColor(i, c);
		strip.show();
		delay(wait);
	}
}

void rainbow(uint8_t wait) {
	uint16_t i, j;

	for(j=0; j<256; j++) {
		for(i=0; i<strip.numPixels(); i++) {
			strip.setPixelColor(i, Wheel((i+j) & 255));
		}
		strip.show();
		delay(wait);
	}
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
	uint16_t i, j;

	for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
		for(i=0; i< strip.numPixels(); i++) {
			strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
		}
		strip.show();
		delay(wait);
	}
}

//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait) {
	for (int j=0; j<10; j++) {  //do 10 cycles of chasing
		for (int q=0; q < 3; q++) {
			for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
				strip.setPixelColor(i+q, c);    //turn every third pixel on
			}
			strip.show();

			delay(wait);

			for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
				strip.setPixelColor(i+q, 0);        //turn every third pixel off
			}
		}
	}
}

//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow(uint8_t wait) {
	for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
		for (int q=0; q < 3; q++) {
			for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
				strip.setPixelColor(i+q, Wheel( (i+j) % 255));    //turn every third pixel on
			}
			strip.show();

			delay(wait);

			for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
				strip.setPixelColor(i+q, 0);        //turn every third pixel off
			}
		}
	}
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
	WheelPos = 255 - WheelPos;
	if(WheelPos < 85) {
		return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
	}
	if(WheelPos < 170) {
		WheelPos -= 85;
		return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
	}
	WheelPos -= 170;
	return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}











void setup()
{
	Serial.begin(115200);
	strip.begin();
	strip.setBrightness(100);
	strip.show(); // Initialize all pixels to 'off'
	Serial.println(BUILTIN_LED);
	setup_wifi();
	client.setServer(mqtt_server, 1883);
	client.setCallback(callback);
	rgb[0]=0;
	rgb[1]=0;
	rgb[2]=0;
}
void loop()
{
	colorWipe(strip.Color(rgb[0], rgb[1], rgb[2]),40);
	if (!client.connected()) {
		reconnect();
	}
	client.loop();
}
